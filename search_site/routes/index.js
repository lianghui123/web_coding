var express = require('express');
var router = express.Router();
var mysql = require('../mysql.js');
var echarts=require('echarts');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/process_get', function(request, response){
  //var fetchSql = "select url,source_name,title,author,publish_date " +  
  //   "from fetches where title like '%" +request.query.title + "%'"+" order by publish_date";
  var fetchSql="select url,source_name,title,author,publish_date from fetches where ";
  var flag=false;
  if(request.query.title=="true"){
    if(!flag){
      flag=true;
    }
    else{
      fetchSql += "or";
    }
    fetchSql+="title like '%"+request.query.words+"%'";
  }

  if(request.query.keywords=="true"){
    if(!flag){
      flag=true;
    }
    else{
      fetchSql += "or";
    }
    fetchSql+="keywords like '%"+request.query.words+"%'";
  }
  if(request.query.content=="true"){
    if(!flag){
      flag=true;
    }
    else{
      fetchSql += "or";
    }
    fetchSql+="content like '%"+request.query.words+"%'";
  }
  fetchSql=fetchSql+"order by publish_date";

  mysql.query(fetchSql, function(err, result, fields){
    response.writeHead(200, {
      "Content-Type": "application/json"
    });
    try{
    response.write(JSON.stringify(result));
    response.end();
  }catch{
    console.log("结果为空");
  }
  });
});
 
router.get('/process_hot',function(req,res){
  var fetchSql="SELECT publish_date,COUNT(*) FROM fetches WHERE title LIKE '%"+req.query.title+"%'"+" GROUP BY publish_date"
  mysql.query(fetchSql,function(err,result,fields){
    res.writeHead(200,{
      "Content-Type":"application/json"
    });
    res.write(JSON.stringify(result));
    res.end();
  });
});

module.exports = router;
